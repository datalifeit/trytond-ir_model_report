# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields, MatchMixin,
    sequence_ordered)
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool


class IrModel(metaclass=PoolMeta):
    __name__ = 'ir.model'

    reports = fields.One2Many('ir.model.report', 'model', 'Reports')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('reports', None)
        return super().copy(records, default=default)

    @classmethod
    def get_report(cls, model_name, report_name, pattern,
            iteration_filter=('origin', )):
        model, = cls.search([
            ('model', '=', model_name)])

        return model._get_report(pattern, report_name,
            iteration_filter=iteration_filter)

    def _get_report(self, pattern, report_name, iteration_filter=('origin', )):
        pattern = pattern.copy()

        def match(pattern):
            for report in self.reports:
                if (report_name == report.action_report.report_name
                        and report.match(pattern, match_none=True)):
                    return report

        value = None
        for item in (None, ) + iteration_filter:
            # remove pattern key on each iteration
            if item:
                pattern[item] = None
            value = match(pattern)
            if value:
                return value.action_report


class IrModelReport(sequence_ordered(), MatchMixin, ModelSQL, ModelView):
    '''Model Report'''
    __name__ = 'ir.model.report'

    model = fields.Many2One('ir.model', 'Model', required=True,
        ondelete='CASCADE')
    model_name = fields.Function(fields.Char('Model name'),
        'on_change_with_model_name')
    action_report = fields.Many2One('ir.action.report', 'Action report',
        required=True,
        domain=[('model', '=', Eval('model_name'))],
        depends=['model_name'])
    origin = fields.Reference('Origin', selection='get_origin')

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = Model.search([])
        return [('', '')] + [(m.model, m.name) for m in models]

    @fields.depends('model', '_parent_model.model')
    def on_change_with_model_name(self, name=None):
        if self.model:
            return self.model.model
