# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.transaction import Transaction
from trytond.pool import Pool


class IrModelReportTestCase(ModuleTestCase):
    """Test Ir Model Report Selection module"""
    module = 'ir_model_report'

    @with_transaction()
    def test_report_matchin(self):
        pool = Pool()
        IrModel = pool.get('ir.model')
        IrReport = pool.get('ir.action.report')
        IrModelReport = pool.get('ir.model.report')
        User = pool.get('res.user')

        user_model, = IrModel.search([
            ('model', '=', 'res.user')])
        user_admin = User(Transaction().user)
        user_root = User(0)

        # user reports
        report1 = IrReport(name='User report 1', model='res.user',
            report_name='res.user.report')
        report1.save()
        report2 = IrReport(name='User report 2', model='res.user',
            report_name='res.user.report')
        report2.save()
        report3 = IrReport(name='User report 3', model='res.user',
            report_name='res.user.report')
        report3.save()
        values = [
            (None, report2),
            (user_admin, report1),
        ]
        for origin, report in values:
            item = IrModelReport(model=user_model,
                origin=origin, action_report=report)
            item.save()

        # check report for user admin
        pattern = {
            'origin': user_admin
        }
        self.assertEqual(IrModel.get_report('res.user', 'res.user.report',
            pattern), report1)

        # check report for user root: default report with no origin
        pattern['origin'] = user_root
        self.assertEqual(IrModel.get_report('res.user', 'res.user.report',
            pattern), report2)

        item = IrModelReport(model=user_model,
                origin=user_root, action_report=report3)
        item.save()

        # check report for user root
        self.assertEqual(IrModel.get_report('res.user', 'res.user.report',
            pattern), report3)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            IrModelReportTestCase))
    return suite
